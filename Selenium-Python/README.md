<h1>Selenium con Python</h1>

<h3>Héctor Vega</h3>

<h1>Tabla de Contenido</h1

- [1. Conocer el ecosistema de Selenium](#1-conocer-el-ecosistema-de-selenium)
  - [Bienvenida al curso](#bienvenida-al-curso)
  - [Historia de Selenium](#historia-de-selenium)
    - [Selenium IDE](#selenium-ide)
  - [Otras herramientas de testing y automatización](#otras-herramientas-de-testing-y-automatización)
- [2. Preparar entorno de trabajo](#2-preparar-entorno-de-trabajo)
  - [Configurar entorno de trabajo](#configurar-entorno-de-trabajo)
  - [Compatibilidad con Python 3.9 y aprendiendo a utilizar múltiples versiones](#compatibilidad-con-python-39-y-aprendiendo-a-utilizar-múltiples-versiones)
    - [¿Por qué utilizar pyenv y entornos virtuales?](#por-qué-utilizar-pyenv-y-entornos-virtuales)
    - [¿Qué versión de Python tengo instalada y dónde?](#qué-versión-de-python-tengo-instalada-y-dónde)
    - [1. Instalando dependencias](#1-instalando-dependencias)
        - [macOS](#macos)
    - [Distribuciones Linux](#distribuciones-linux)
    - [2. Instalando pyenv y Python](#2-instalando-pyenv-y-python)
      - [Listar versiones de Python](#listar-versiones-de-python)
    - [Verificando versiones instaladas](#verificando-versiones-instaladas)
      - [Desinstalando versiones de Python](#desinstalando-versiones-de-python)
    - [3. Utilizando las nuevas versiones instaladas](#3-utilizando-las-nuevas-versiones-instaladas)
    - [4. Utiliza una versión de Python específica en un entorno virtual](#4-utiliza-una-versión-de-python-específica-en-un-entorno-virtual)
      - [Seleccionemos una versión de Python](#seleccionemos-una-versión-de-python)
    - [Creando el entorno virtual](#creando-el-entorno-virtual)
  - [¡Hola, mundo!](#hola-mundo)
    - [Unittest (pytest)](#unittest-pytest)
- [3. Utilizar comandos básicos](#3-utilizar-comandos-básicos)
  - [Encontrar elementos con find_element](#encontrar-elementos-con-find_element)
  - [Preparar assertions y test suites](#preparar-assertions-y-test-suites)
  - [Entender las clases WebDriver y WebElement](#entender-las-clases-webdriver-y-webelement)
- [4. Interactuar con elementos](#4-interactuar-con-elementos)
  - [Manejar form, textbox, checkbox y radio button](#manejar-form-textbox-checkbox-y-radio-button)
  - [Manejar dropdown y listas](#manejar-dropdown-y-listas)
  - [Manejar alert y pop-up](#manejar-alert-y-pop-up)
  - [Automatizar navegación](#automatizar-navegación)
- [5. incronizar pruebas](#5-incronizar-pruebas)
  - [Demora implícita y explícita](#demora-implícita-y-explícita)
  - [Condicionales esperadas](#condicionales-esperadas)
- [6. Retos](#6-retos)
  - [Agregar y eliminar elementos](#agregar-y-eliminar-elementos)
  - [Elementos dinámicos](#elementos-dinámicos)
  - [Controles dinámicos](#controles-dinámicos)
  - [Typos](#typos)
  - [Ordenar tablas](#ordenar-tablas)
- [7. Metodologías de Trabajo](#7-metodologías-de-trabajo)
  - [Data Driven Testing (DDT)](#data-driven-testing-ddt)
  - [Page Object Model (POM)](#page-object-model-pom)
- [8. Cierre del curso](#8-cierre-del-curso)
  - [Realizar una prueba técnica](#realizar-una-prueba-técnica)
  - [Conclusiones](#conclusiones)

# 1. Conocer el ecosistema de Selenium

## Bienvenida al curso

- [slides-curso-de-selenium-con-python.pdf](https://drive.google.com/file/d/1Fm70tCqy5bOnSsO45vt2bl6qTYlo8VNF/view?usp=sharing)

## Historia de Selenium

¿Qué es Selenium?
- Es una **UIT de herramientas** para la automatización de navegadores Web.
- El objetivo de Selenium NO fue para el Testing ni para el **Web Scraping** (aunque se puede usar para eso), por lo tanto, no es el más optimo para estas actividades.
- Protocolo: WebDriver, herramienta que se conecta a un API.
- **Selenium WebDriver** es la herramienta que utilizaremos en el curso.
- **Selenium** NO es un Software, ES una SUIT de Softwares.
- **DDT Data Drive Testing:** Ingresar datos para que realice varias pruebas (sin intervención humana).

### Selenium IDE

**Pros**

  - Excelente para iniciar
  - No requiere saber programar
  - Exporta scripts para Selenium RC y Selenium WebDriver
  - Genera reportes

**Contras**

  - Disponible para Google Chrome y FireFox
  - No soporta DDT. No permite colocar datos para múltiples pruebas.
  - Selenium RC

**Pros**

  - Soporte para
  - Varias plataformas, navegadores y lenguajes.
  - Operaciones lógicas y condicionales
  - DDT
  - Posee una API madura

**Contras**

  - Complejo de instalar
  - Necesita de un servidor corriendo.
  - Comandos redundantes en una API
  - Navegación no tan realista
  - Selenium Web Driven

**Pros**

  - Soporte para múltiples lenguajes
  - Facil de instalar.
  - Comunicación directa con el navegador.
  - Interacción más realista.

**Contra**

  - No soporta nuevos navegadores tan rápido.
  - No genera reportes o resultados de pruebas.
  - Requiere de saber programar.

## Otras herramientas de testing y automatización

Algunas herramientas de testing y automatización:

**[Puppeteer:](https://pptr.dev/)**

• PROS: Soporte por parte de Google, te brinda datos del Performance Analysis de Chrome y un mayor control de este navegador. No requiere archivos externos como lo hace Selenium con WebDriver.

• CONTRAS: Solo funciona para Google Chrome con JavaScript, tiene una comunidad pequeña así que el apoyo será poco.

**[Cypress.io:](https://www.cypress.io/)**

• PROS: Tiene una comunidad emergente y va creciendo a pasos acelerados, tiene muy buena documentación para implementar Cypress en los proyectos. Es muy ágil en pruebas E2E, está orientado a desarrolladores y tiene un excelente manejo del asincronismo, logrando que las esperas sean dinámicas y también se puedan manejar fácilmente.

• CONTRAS: Solo funciona en Google Chrome con JavaScript, se pueden realizar pruebas en paralelo únicamente en la versión de pago.

Factores a tomar encenta para escoger una herramienta de automatización:

- Lenguaje con el que estas más familiarizado.
- Que lenguaje de programación ocupa el equipo de trabajo.
- Sí el proyecto necesita llamadas de a sincronismo, o no.
- O solo hacer una automatización de una tarea repetitiva.

# 2. Preparar entorno de trabajo

## Configurar entorno de trabajo

Instalar python3 en su version 7 o superior.

``` bash
sudo pacman -S python
```

Instalar Selenium
``` BASH
pip install selenium
```

Instalar pyunitreport
``` BASH
pip3 install pyunitreport
```

## Compatibilidad con Python 3.9 y aprendiendo a utilizar múltiples versiones

Durante este curso utilizaremos Selenium 3, la cual de momento no es compatible con Python 3.9 y no será sino Selenium 4 que nos brindará esta compatibilidad cuando sea lanzado, ya que por ahora se encuentra en una versión alpha.

Lo recomendable es que durante el curso utilices Python en una versión entre 3.6 y 3.8, puede sonar un poco extraño trabajar con versiones anteriores a la más reciente y en esta clase te enseñaré a manejar distintas versiones de Python en tu equipo.

En caso de que tengas una versión de Python inferior a 3.9 compatible con Selenium 3 puedes omitir los siguientes pasos y continuar con el curso. Recuerda que como buena práctica todo lo que instales a través de pip debe ser en un entorno virtual.

### ¿Por qué utilizar pyenv y entornos virtuales?

Sí, puedes instalar una versión distinta directo en tu sistema, continuar con el curso (quizá lo termines sin problemas) y pretender que no ha sucedido nada. Sin embargo cuando instalas una versión de Python directo al sistema operativa esta funcionará de forma global y lo mismo con los módulos que instales a través de pip (a no ser que uses un entorno virtual).

Cuando instales paquetes de forma global en cada versión de Python en tu sistema, es probable que estos lleguen a ser de versiones distintas y es aquí cuando los problemas se pueden presentar. Por ello es mejor tener cada cosa en su lugar, separada y sin afectar otros entornos.

### ¿Qué versión de Python tengo instalada y dónde?

Utilizar distintas versiones de Python en tu equipo puede generar problemas cuando haces uso de pip para instalar paquetes, pues estos se instalan de forma global y al haber versiones distintas del mismo te expones a errores. Recuerda que como buena práctica lo mejor es instalar paquetes en entornos virtuales.

Para saber donde está instalado Python en tu equipo ejecutas el siguiente comando desde tu terminal:

```bash
which python
```

Te dará como resultado algo similar a esto:

```bash
/usr/local/bin/python3.9
```

Si quieres saber exactamente que versión de Python está instalada en tu sistema operativo, ejecuta el siguiente comando:

``` bash
python --version
```

Esto te mostrará exactamente la versión de tu sistema:

``` bash
Python 3.9.0
```

### 1. Instalando dependencias

Lo ideal es utilizar pyenv pues nos permite levantar Python desde su fuente, para lo cual debemos instalar ciertas dependencias de acuerdo a nuestro sistema operativo:

##### macOS

En caso de tener macOS Mojave o superior (+10.14) debes utilizar el siguiente comando con Homebrew instalado:

``` bash
sudo installer -pkg /Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_10.14.pkg -target /
```

Si es una versión inferior, será el siguiente comando:

``` bash
brew install openssl readline sqlite3 xz zlib.
```

### Distribuciones Linux

Según la distribución de tu equipo será el comando que utilizarás:

  - **Debian/Ubuntu**

``` bash
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl
```

  - **Fedora/CentOS/RHEL**

``` bash
sudo yum install zlib-devel bzip2 bzip2-devel readline-devel sqlite \
sqlite-devel openssl-devel xz xz-devel libffi-devel
```

  - **openSUSE**

``` bash
zypper in zlib-devel bzip2 libbz2-devel libffi-devel \
libopenssl-devel readline-devel sqlite3 sqlite3-devel xz xz-devel
```

  - **Windows**

Actualmente pyenv no cuenta con soporte para Windows, así que lo mejor será utilizar WSL (Windows Subsystem for Linux)

### 2. Instalando pyenv y Python

Instalar pyenv es muy sencillo, solo debes ejecutar este comando en tu terminal:

``` bash
curl https://pyenv.run | bash
```

Posterior a instalar pyenv reinicia tu terminal para continuar.

#### Listar versiones de Python

Lo ideal es instalar una versión de Python entre 3.6 y 3.8, puedes ver la lista disponible con este comando una vez instalado pyenv:

``` bash
pyenv install --list | grep " 3\.[678]"
```

Después solo debes elegir, por ejemplo acá elegimos Python 3.8.6:

``` bash
pyenv install -v 3.8.6
```

Con esto pyenv procederá a levantar la versión de Python desde su fuente indicada y realizar las descargas necesarias, por lo que puede tomar unos minutos.

### Instalar las dependecias

file `requeriments.txt`

```
pip install -r requeriments.txt
```

### Verificando versiones instaladas

Puedes saber que versiones de Python has instalado mediante pyenv con este comando:

``` bash
ls ~/.pyenv/versions/
```

#### Desinstalando versiones de Python

Ok, esto será necesario en algún momento y es posible con un sencillo comando donde solo debes especificar la versión a remover:

``` bash
pyenv uninstall 3.8.6
```

## Creando entornos virtuales 💻

Aislar los paquetes que utiliza un proyecto, podemos utilizar entornos virtuales. Crea uno usando la `pyenv-virtualenv`extensión (consulte las instrucciones de instalación a continuación):

```sh
pyenv virtualenv 3.8.0 my-data-project
```

Puede nombrar el entorno (aquí `my-data-project`) como más le convenga. Para activar el tipo de entorno:

```sh
pyenv shell my-data-project
```

Ahora está listo para instalar las bibliotecas que le gustaría usar. Puede extraerlos del repositorio de paquetes estándar de Python (PyPi también llamado tienda de quesos) e instalarlos en el entorno actual usando el `pip`comando:

```sh
pip install jupyter notebook matplotlib pandas
```

Es una buena práctica instalar solo las bibliotecas que va a utilizar en el proyecto. Si más adelante se da cuenta de que falta algo, aún puede agregar los paquetes con `pip install`. Ahora, iniciemos el servidor del portátil:

```sh
jupyter notebook
```

Esto debería abrir la interfaz del portátil Jupyter en el navegador. ¡Bien hecho! ¡Acaba de instalar Jupyter ejecutándose en la última y mejor versión de Python 🎉! Asegúrese de probar algunas de sus nuevas funciones, como el operador Walrus o las expresiones de f-string .

También puede enumerar todas las versiones de Python y el entorno instalado en su sistema con `pyenv versions`, que debería imprimir una lista como:

```sh
system
3.8.0
3.8.0/envs/my-data-project
```

### Entornos de activación automática 🚀

```sh
# Activar pyenv
pyenv activate venv

# Desconectar
source deactivate
```

#### Extraer

Extraer archivos `zip`

```sh
unzip -d /tmp ~/Downloads/chromedriver*.zip
```

mover al directorio correspondiente `ejemplo`

```sh
sudo mv /tmp/chromedriver /usr/local/bin
```

#### Error al ejecutar el archivo `.py`

```sh
----------------------------------------------------------------------
Ran 1 test in 1.093s

FAILED
 (Errors=1)

Generating HTML reports... 
Template is not specified, load default template instead.
Reports generated: /home/hades/Documents/platzi_learning/python/Selenium-Python/platzi-selenium/reports/reports/hello_world-report.html

```

- Verificar la version del Navegador [`google-chrome`](google-chrome-stable aur)

  ```sh
  
  
  Google Chrome	96.0.4664.110 (Official Build) (64-bit)
  Revision	d5ef0e8214bc14c9b5bbf69a1515e431394c62a6-refs/branch-heads/4664@{#1283}
  OS	Linux
  JavaScript	V8 9.6.180.21
  User Agent	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36
  Command Line	/opt/google/chrome/google-chrome --enable-crashpad --flag-switches-begin --flag-switches-end
  Executable Path	/opt/google/chrome/google-chrome
  Profile Path	/home/hades/.config/google-chrome/Default
  ```

- Descargar la Version [`chromedriver`](https://chromedriver.chromium.org/downloads) correspondiente

  ```sh
  # Utilizo google-chrome 96.0.46... descargar chromedriver la version correspondiente | luego todo funciona perfecto
  ChromeDriver 96.0.4664.18
  
  Supports Chrome version 96
  
  Resolved issue 3445: Impossible to access elements in iframe inside a shadow root [Pri-3]
  ```

### 3. Utilizando las nuevas versiones instaladas

**¿En qué versión me encuentro?**

Ser consciente de que versión de Python es importante, así podemos utilizar las herramientas que son compatibles (como Selenium) y el comando indicado es el siguiente:

``` bash
pyenv versions
```

Esto mostrará tanto nuestra versión instalada en el sistema operativa, su ruta y las que instalamos con pyenv. Podrás ver que está marcado con un * aquella que está activa de momento. Esto se puede validar con el comando python --version.

También puedes saber la ruta de la versión de Python tiene activa pyenv con el comando pyenv which python.

**Cambiando de versión**

Suponiendo que tenemos instalado Python 3.9, ya instalamos Python 3.8 con pyenv y queremos cambiar solo hay que ejecutar el siguiente comando:

``` bash
pyenv global 3.8.6
```

Puedes verificarlo ejecutando el comando python --version el cual te mostrará la versión activa.

**Volviendo a la versión del sistema operativo**

En caso de necesitar volver a la versión de nuestro sistema operativo solo ejecutamos este comando:

``` sh
pyenv global system
```

Definiendo una versión por defecto en directorios
En ocasiones deberás trabajar con una versión específica para tu proyecto, así que lo mejor es que pyenv sepa de cuál se trata y cada vez que trabajes desde un directorio esté activada. Esto lo puedes realizar con el comando local, por ejemplo, supongamos que queremos Python 3.7.6 por defecto entonces ejecutamos este comando:

``` bash
pyenv local 3.7.6
```

Esto generará el archivo .python_version, mismo que fijará la versión 3.7.6 de Python en el directorio donde se ubique y todos los demás que se contengan en este. Si lo eliminas, entonces podrás cambiar entre versiones con el comando global.

### 4. Utiliza una versión de Python específica en un entorno virtual
Nuestra versión de Python específica está lista para funcionar, lo siguiente es crear un entorno virtual con pyenv donde trabajaremos con esta y poder realizar la instalación de cualquier módulo con pip.

Considera que pyenv nos permite manejar y cambiar entre distintas versiones de Python, los entornos virtuales aislan las instalaciones de diversos módulos y juntos nos permiten aislar módulos para versiones específicas de Python.

#### Seleccionemos una versión de Python

Lo primero será ubicarnos al directorio donde estarán nuestro proyecto y elegir la versión preferida con el comandolocal (de preferencia), por ejemplo 3.8.6:

``` bash
pyenv local 3.8.6
```

### Creando el entorno virtual

Generar un nuevo entorno local es posible con la siguiente convención:

``` bash
pyenv virtualenv <version_de_python> <nombre_del_entorno>
```

Siguiendo el ejemplo de Python 3.8.6 lo haremos así:

``` bash
pyenv virtualenv 3.8.6 curso-selenium-platzi
```

Para activar nuestro entorno virtual con esta versión específica de Python solo ejecutamos el comando local con el nombre del entorno:

``` bash
pyenv local curso-selenium-platzi
```

Podemos utilizar `pyenv versions` y `pyenv which python` para darnos cuenta de que estamos utilizando la `versión 3.8.6` en el directorio de nuestro entorno virtual.

Incluso ahora podemos instalar Selenium de una forma aislada con el comando `pip install selenium`.

Para salir de nuestro entorno solo ejecutamos el comando `pyenv deactivate` y para activarlo de nuevo con `pyenv activate nombre_del_entorno`.

## ¡Hola, mundo!

`hello_world.py`

```python
import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver

class HelloWorld(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path = r'./chromedriver')
    driver = self.driver
    driver.implicitly_wait(10)

  def test_hello_world(self):
    driver = self.driver
    driver.get('https://www.platzi.com')

  def test_visit_wikipedia(self):
    self.driver.get('https://www.wikipedia.org')

  def tearDown(self):
    self.driver.quit()

if __name__ == "__main__":
  unittest.main(verbosity = 2, testRunner = HTMLTestRunner(output = 'report', report_name = 'hello-world-report'))
```

Ejecutamos nuestro archivo `.py`

```sh
python3 hello_world
```



### Unittest (pytest)

- **Test Fixture:** Preparaciones para antes y despues de la prueba.
- **Test Case:** Unidad de codigo a probar.
- **Test Suite:** Coleccion de Test Cases.
- **Test Runner:** Orquestador de la ejecucion. 
- **Test Report:** Resumen de resultado

[![img](https://www.google.com/s2/favicons?domain=//www.google.com/images/icons/product/sites-16.ico)ChromeDriver - WebDriver for Chrome](http://chromedriver.chromium.org/)

# 3. Utilizar comandos básicos

## Encontrar elementos con find_element

En ocasiones algunos sitios pueden tener bloqueos regionales o no estar disponibles por la alta cantidad de solicitudes que llegan a tener.
Si el sitio de práctica no abre, puedes intentar ingresando a [OneStepCheckout Responsive Demo](http://demo.onestepcheckout.com/).

Usualmente las personas que hemos trabajado con selenium seguimos una prioridad para encontrar elementos.

En mi caso yo uso la siguiente.

- Id
- Name
- CSS selector o XPATH
- Link Text o PartialLinkText
- class

**código** :

```python
import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver

class EcomerceHomePage(unittest.TestCase):

def setUp(self):
    self.driver = webdriver.Chrome(executable_path= r'C:\Aprendiendo\Selenium\chromedriver.exe')
    driver = self.driver
    driver.get("http://demo-store.seleniumacademy.com")
    driver.maximize_window()
    driver.implicitly_wait(10)
    
def test_search_text_field(self):
    search_field = self.driver.find_element_by_id("search")
    
def test_search_text_field_by_name(self):
    search_field = self.driver.find_element_by_name("q")
    
def test_search_text_field_by_class(self):
    search_field = self.driver.find_element_by_class_name("input-text")
    
def test_search_button_enabled(self):
    button = self.driver.find_element_by_class_name("input-text")   

def test_count_of_promo_bar(self):
    banner_list = self.driver.find_element_by_class_name("promos")   
    banners = banner_list.find_elements_by_tag_name('img')
    self.assertEqual(3,len(banners))
    
def test_vip_promo(self):
    vip_promo = self.driver.find_element_by_xpath('//*[@id="top"]/body/div/div[2]/div[2]/div/div/div[2]/div[1]/ul/li[4]/a/img')     
    
def test_shopping_cart(self):
    shopping_cart_icon = self.driver.find_element_by_css_selector("div.header-minicart span.icon") 

def tearDown(self):
    self.driver.quit()

if **name** == “**main**”:
unittest.main(verbosity = 2, testRunner = HTMLTestRunner(output= ‘reportes’,report_name= ‘Ecomerce_report’))
```

En Selenium, podemos usar los selectores de las páginas web para llegar a los elementos, como son:

- ID
- Nombre del atributo
- Nombre de la clase
- Nombre de la etiqueta
- XPath -> Ruta de nodos en XML que indica la ubicación exacta de dónde se encuentra un elemento. NO es la mejor opción, pues los elementos de una página web pueden cambiar.
- Selector de CSS
- Texto del link
- Texto parcial del link

Podemos practicar en Madison Island

Instrucciones usadas en esta clase:

- `self.driver.find_element_by_id` -> Encontrar elemento por su ID
- `self.driver.find_element_by_name` -> Encontrar elemento por su name
- `self.driver.find_element_by_class_name` -> Encontrar elemento por nombre de clase CSS
- `find_elements_by_tag_name` -> Encontrar elementos por sus etiquetas HTML
- `self.driver.find_element_by_xpath` -> Encontrar elemento por su XPATH
- `self.driver.find_element_by_css_selector` -> Encontrar elemento por su selector CSS

Código de la clase (en WSL):

search_test.py

```python
import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class HelloWorld(unittest.TestCase):

  @classmethod
  def setUpClass(cls):
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    cls.driver = webdriver.Chrome(executable_path = '/usr/bin/chromedriver' , options=options)
    driver = cls.driver
    driver.get('ENLACE_DE_LA_WEB_QUE_PLATZI_NO_ME_DEJA_ESCRIBIR_AQUI')
    #driver.maximize_window()
    #driver.implicitly_wait(15) # segundos


  def test_search_text_fild(self):
    search_field = self.driver.find_element_by_id("search")


  def test_search_text_field_by_name(self):
    search_field = self.driver.find_element_by_name("q")


  def test_search_text_field_class_name(self):
    search_field = self.driver.find_element_by_class_name("input-text")


  def test_search_button_enabled(self):
    button = self.driver.find_element_by_class_name("button")


  def test_count_of_promo_banner_images(self):
    banner_list = self.driver.find_element_by_class_name("promos") #Buscamos la clase "promos"
    banners = banner_list.find_elements_by_tag_name("img") #Buscamos las etiquetas img dentro de promos
    self.assertEqual(3, len(banners)) #Hacemos una assertion para ver si efectivamente es la cantidad de imágenes que esperamos
    #! NO son 3 imágenes, son 4, pero recordemos que se cuenta: [0,1,2,3]


  def test_vip_promo(self):
    vip_promo = self.driver.find_element_by_xpath('//*[@id="top"]/body/div/div[2]/div[2]/div/div/div[2]/div[1]/ul/li[4]/a/img')


  def test_shopping_cart(self):
    shopping_cart_icon = self.driver.find_element_by_css_selector("div.header-minicart span.icon")


  @classmethod
  def tearDownClass(cls):
    cls.driver.quit()


if __name__ == '__main__':
  unittest.main(verbosity = 2)
```

![Estructura de un sitio web  + Selectores.png](https://static.platzi.com/media/user_upload/Estructura%20de%20un%20sitio%20web%20%20%2B%20Selectores-bec27b1e-4b8c-4199-927b-478336f047f1.jpg)

[![img](https://www.google.com/s2/favicons?domain=http://demo-store.seleniumacademy.com/skin/frontend/base/default/favicon.ico)Madison Islandhttp://demo-store.se](http://demo-store.seleniumacademy.com/)

[![img](https://www.google.com/s2/favicons?domain=https://www.techbeamers.com/wp-content/uploads/2020/03/favicon_tb.ico)7 Websites to Practice Selenium Webdriver Onlineh](https://www.techbeamers.com/websites-to-practice-selenium-webdriver-online/)

[![img](https://www.google.com/s2/favicons?domain=http://demo.onestepcheckout.com/skin/frontend/base/default/favicon.ico)OneStepCheckout Responsive Demo](http://demo.onestepcheckout.com/)

## Preparar assertions y test suites

**Código ya con documentación.**

**Assertions**

```python
import unittest
from selenium import webdriver
#sirve como excepción para los assertions cuando queremos
#validar la presencia de un elemento
from selenium.common.exceptions import NoSuchElementException
#ayuda a llamar a las excepciones que queremos validar
from selenium.webdriver.common.by import By

classAssertionsTest(unittest.TestCase):

	defsetUp(self):
		self.driver = webdriver.Chrome(executable_path = r'.\chromedriver.exe')
		driver = self.driver
		driver.implicitly_wait(30)
		driver.maximize_window()
		driver.get("http://demo.onestepcheckout.com/")

	deftest_search_field(self):
		self.assertTrue(self.is_element_present(By.NAME, 'q'))

	deftest_language_option(self):
		self.assertTrue(self.is_element_present(By.ID, 'select-language'))

	deftearDown(self):
		self.driver.quit()

	#para saber si está presente el elemento
	#how: tipo de selector
	#what: el valor que tiene
	def	is_element_present(self, how, what):
		try:  #busca los elementos según el parámetro
			self.driver.find_element(by = how, value = what) 
		except NoSuchElementException as variable:
			returnFalse
		returnTrue
```

**Search tests**

```python
import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver

classSearchTests(unittest.TestCase):
	defsetUp(self):
		self.driver = webdriver.Chrome(executable_path = r'.\chromedriver.exe')
		driver = self.driver
		driver.implicitly_wait(30)
		driver.maximize_window()
		driver.get("http://demo.onestepcheckout.com")

	deftest_search_tee(self):
		driver = self.driver
		search_field = driver.find_element_by_name('q')
		search_field.clear() #limpia el campo de búsqueda en caso de que haya algún texto. 
		
		search_field.send_keys('tee') #simulamos la escritura del teclado para poner "tee"
		search_field.submit() #envía los datos ('tee') para que la página muestre los resultados de "tee"
		
	deftest_search_salt_shaker(self):
		driver = self.driver
		search_field = driver.find_element_by_name('q')
		
		search_field.send_keys('salt shaker') #escribimos 'salt shaker' en la barra de búsqueda
		search_field.submit() #envíamos la petición

		#hago una lista de los resultados buscando los elementos por su Xpath. Es la forma más rápida.
		products = driver.find_elements_by_xpath('//*[@id="top"]/body/div/div[2]/div[2]/div/div[2]/div[2]/div[3]/ul/li/div/h2/a')

		#vamos a preguntar si la cantidad de resultados es igual a 1
		self.assertEqual(1, len(products))
		
	deftearDown(self):
		self.driver.quit()
```

**Smoke tests**

```python
from unittest import TestLoader, TestSuite
from pyunitreport import HTMLTestRunner #para generar el reporte
from assertions import AssertionsTest
from search_tests_assertions import SearchTests


assertions_test = TestLoader().loadTestsFromTestCase(AssertionsTest)
search_tests = TestLoader().loadTestsFromTestCase(SearchTests)

#contruimos la suite de pruebas
smoke_test = TestSuite([assertions_test, search_tests])


#para generar los reporters
kwargs = {
    "output": 'smoke-report'
    }

#la variable runner almacena un reporte generado por HTMLTestRuner
#usa como argumento "kwarsp"
runner = HTMLTestRunner(**kwargs)

#corro el rurner con la suite de prueba
runner.run(smoke_test) 
```

Linux 

`assertions.py`

```python
import unittest
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


class AssertionsTest(unittest.TestCase):

	def setUp(self):
		self.driver = webdriver.Chrome(executable_path = r'./chromedriver')
		driver = self.driver
		driver.implicitly_wait(30)
		driver.maximize_window()
		driver.get("http://demo-store.seleniumacademy.com/")

	def test_search_field(self):
		self.assertTrue(self.is_element_present(By.NAME, 'q'))

	def test_language_option(self):
		self.assertTrue(self.is_element_present(By.ID, 'select-language'))

	def tearDown(self):
		self.driver.quit()

	def is_element_present(self, how, what):
		try:
				self.driver.find_element(by=how, value=what)
		except NoSuchElementException as variable:
				return False
		return True

```

`searchtests.py`

```python
import unittest
from selenium import webdriver

class HomePageTests(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path = r'./chromedriver')
    driver = self.driver
    driver.get("http://demo-store.seleniumacademy.com/")
    driver.maximize_window()
    driver.implicitly_wait(15)

  def test_search_text_field(self):
    search_field = self.driver.find_element_by_id("search")

  def test_search_text_field_by_name(self):
    search_field =  self.driver.find_element_by_name("q")

  def test_search_text_field_class_name(self):
    search_field = self.driver.find_element_by_class_name("input-text");

  def test_search_button_enabled(self):
    button = self.driver.find_element_by_class_name("button")

  def tearDown(self):
    self.driver.quit()

  def test_count_of_promo_banner_images(self):
    banner_list = self.driver.find_element_by_class_name("promos")
    banners = banner_list.find_elements_by_tag_name('img')
    self.assertEqual(3, len(banners))

  def test_vip_promo(self):
    vip_promo = self.driver.find_element_by_xpath('//*[@id="top"]/body/div/div[2]/div[2]/div/div/div[2]/div[1]/ul/li[4]/a/img')
  
  def test_shopping_cart(self):
    shopping_cart_icon = self.driver.find_element_by_css_selector("div.header-minicart span.icon") 


if __name__ == "__main__":
  unittest.main(verbosity = 2)

```

`smoketests.py`

```python
from unittest import TestLoader, TestSuite
from pyunitreport import HTMLTestRunner
from assertions import AssertionsTest
from searchtests import SearchTests


assertions_test = TestLoader().loadTestsFromTestCase(AssertionsTest)
search_tests = TestLoader().loadTestsFromTestCase(SearchTests)

#Construccion de suite de pruebas
smoke_test = TestSuite([assertions_test, search_tests])

#Respecto a los reportes

kwargs = {
  "output": 'smoke-report'
}

runner = HTMLTestRunner(**kwargs)
runner.run(smoke_test)

```



## Entender las clases WebDriver y WebElement

Como viste en clases anteriores, un sitio web se construye por código **HTML** en forma de árbol, conteniendo distintos elementos con los que podemos interactuar según estén presentes o no en nuestra interfaz gráfica.

**Selenium WebDriver** nos brinda la posibilidad de poder referirnos a estos elementos y ejecutar métodos específicos para realizar las mismas acciones que un humano haría sobre los mismos, gracias a las clases **WebDriver** y **WebElement**.

## Clase WebDriver

Cuenta con una serie de propiedades y métodos para interactuar directamente con la ventana del navegador y sus elementos relacionados, como son pop-ups o alerts. Por ahora nos centraremos a las más utilizadas.

## Propiedades de la clase WebDriver

Estas son las más comunes para acceder al navegador.

| Propiedad/Atributo    | Descripción                                                  | Ejemplo                            |
| --------------------- | ------------------------------------------------------------ | ---------------------------------- |
| current_url           | Obtiene la URL del sitio en la que se encuentra el navegador | driver.get_url                     |
| current_window_handle | Obtiene la referencia que identifica a la ventana activa en ese momento | driver.current_window_handle       |
| name                  | Obtiene el nombre del navegador subyacente para la instancia activa | [driver.name](http://driver.name/) |
| orientation           | Obtiene la orientación actual del dispositivo móvil          | driver.orientation                 |
| page_source           | Obtiene el código fuente de disponible del sitio web         | driver.page_source                 |
| title                 | Obtiene el valor de la etiqueta <title> del sitio web        | driver.title                       |

## Clase WebElement

Esta clase nos permite interactuar específicamente con elementos de los sitios web como textbox, text area, button, radio button, checkbox, etc.

## Propiedades más comunes de la clase WebElement

| Propiedad/Atributo | Descripción                                        | Ejemplo        |
| ------------------ | -------------------------------------------------- | -------------- |
| size               | Obtiene el tamaño del elemento                     | login.size     |
| tag_name           | Obtiene el nombre de la etiqueta HTML del elemento | login.tag_name |
| text               | Obtiene el texto del elemento                      | login.text     |

## Métodos más comunes de la clase WebElement

| Método/Atributo                      | Descripción                                                  | Ejemplo                                                      |
| ------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| clear()                              | Limpia el contenido de un textarea                           | first_name.clear()                                           |
| click()                              | Hace clic en el elemento                                     | send_button.click()                                          |
| get_attribute(name)                  | Obtiene el valor del atributo de un elemento                 | submit_button.get_attribute(‘value’) last_name.get_attribute(max_length) |
| is_displayed()                       | Verifica si el elemento está a la vista al usuario           | banner.is_displayed()                                        |
| is_enabled()                         | Verifica si el elemento está habilitado                      | radio_button.is_enabled()                                    |
| is_selected()                        | Verifica si el elemento está seleccionado, para el caso de checkbox o radio button | checkbox.is_selected()                                       |
| send_keys(value)                     | Simula escribir o presionar teclas en un elemento            | email_field.send_keys(‘team@platzi.com’)                     |
| submit()                             | Envía un formulario o confirmación en un text area           | search_field.submit()                                        |
| value_of_css_property(property_name) | Obtiene el valor de una propiedad CSS del elemento           | header.value_of_css_property(‘background-color’)             |

# 4. Interactuar con elementos

## Manejar form, textbox, checkbox y radio button

Código de la clase ya con documentación

```python
import unittest
from selenium import webdriver

class RegisterNewUser(unittest.TestCase):

	def setUp(self):
		self.driver = webdriver.Chrome(executable_path = r'.\chromedriver.exe')
		driver = self.driver
		driver.implicitly_wait(30)
		driver.maximize_window()
		driver.get("http://demo.onestepcheckout.com/")
	
	def test_new_user(self):
		driver = self.driver
		#le decimos al driver que encuentre la opción de cuenta por su Xpath y le haga click para desplegar el menu
		driver.find_element_by_xpath('/html/body/div/div[2]/header/div/div[2]/div/a/span[2]').click()
		#el driver va a buscar el enlace por su texto y haga click
		driver.find_element_by_link_text('Log In').click()
		#creo una variable asociada al botón de crear cuenta
		create_account_button = driver.find_element_by_xpath('/html/body/div/div[2]/div[2]/div/div/div[2]/form/div/div[1]/div[2]/a/span/span')
		
		#validamos que el botón esté visible y habilitado
		self.assertTrue(create_account_button.is_displayed() and create_account_button.is_enabled())
		create_account_button.click()

		#comprueba que estamos en el sitio de crear cuenta
		self.assertEqual('Create New Customer Account', driver.title)

		#creación de variables con el nombre del selector correspondiente
		first_name = driver.find_element_by_id('firstname')
		last_name = driver.find_element_by_id('lastname')
		email_address = driver.find_element_by_id('email_address')
		password = driver.find_element_by_id('password')
		confirm_password = driver.find_element_by_id('confirmation')
		news_letter_subscription = driver.find_element_by_id('is_subscribed')
		submit_button = driver.find_element_by_xpath('/html/body/div/div[2]/div[2]/div/div/div[2]/form/div[2]/button')

		#veremos si los elementos están habilitados
		self.assertTrue(first_name.is_enabled() 
		and last_name.is_enabled()
		and email_address.is_enabled()
		and password.is_enabled()
		and confirm_password.is_enabled()
		and news_letter_subscription.is_enabled()
		and submit_button.is_enabled())

		#mandamos los datos al formulario
		first_name.send_keys('Test')
		last_name.send_keys('Test')
		email_address.send_keys('arqcftlothxuknlxkt@awdrt.com') #sacado de 10-minute mail
		password.send_keys('Test')
		confirm_password.send_keys('Test')
		submit_button.click()

	def tearDown(self):
		self.driver.implicitly_wait(3)
		self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)
```

**Codigo mas corto**

```python
import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver

class RegisterNestedw(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='./chromedriver')
        driver = self.driver
        driver.implicitly_wait(30)
        driver.maximize_window()
        driver.get("https://demo-store.seleniumacademy.com/") #quiten la s en 'https', la coloque porque no me dejaba gacer la publicacion 

    def test_new_user(self):
        driver = self.driver
        account_button = driver.find_element_by_xpath('//*[@id="header"]/div/div[2]/div/a/span[2]')
        account_button.click()

        register_button = driver.find_element_by_xpath('//*[@id="header-account"]/div/ul/li[5]/a')
        self.assertTrue(register_button.is_displayed() and register_button.is_enabled())
        register_button.click()

        self.assertEqual('Create New Customer Account', driver.title)

        form_list = driver.find_element_by_class_name('form-list')
        text_fields=form_list.find_elements_by_tag_name('input')
        field_names = [element.get_attribute('id') for element in text_fields]
        input_elements = {field_name:driver.find_element_by_id(field_name) for field_name in field_names}

        for field_name, element in input_elements.items():
            self.assertTrue(element.is_enabled())

        new_user = {'firstname':'Jhon',
                    'middlename':'Doe',
                    'lastname':'Jhonson',
                    'email_address':'asdg@gmail.com',
                    'password':'fetch123',
                    'confirmation':'fetch123'}

        for keys, values in new_user.items():
            if keys == 'is_subscribed':
                input_elements[keys].click()
            else:
                input_elements[keys].send_keys(values)

        button = driver.find_element_by_xpath('//*[@id="form-validate"]/div[2]/button')
        button.click()

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main(verbosity=2)
```

## Manejar dropdown y listas

`select_language.py`

```python
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select

class LanguageOptions(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path = r'./chromedriver')
    driver = self.driver
    driver.implicitly_wait(30)
    driver.maximize_window()
    driver.get('http://demo-store.seleniumacademy.com')

  def test_select_language(self):
    exp_options = ['English', 'French', 'German']
    act_options = []

    select_language = Select(self.driver.find_element_by_id('select-language'))

    self.assertEqual(3, len(select_language.options))

    for option in select_language.options:
      act_options.append(option.text)

    self.assertListEqual(exp_options, act_options)

    self.assertEqual('English', select_language.first_selected_option.text)

    select_language.select_by_visible_text('German')

    self.assertTrue('store=german' in self.driver.current_url)

    select_language = Select(self.driver.find_element_by_id('select-language'))
    select_language.select_by_index(0)


  def tearDown(self):
    self.driver.implicitly_wait(3)
    self.driver.close()


if __name__ == "__main__":
	unittest.main(verbosity=2)
```

codigo con comentarios

```python
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select#Modulo para manejar elementos dropdown

class LanguageOptions(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(executable_path= r'./geckodriver.exe')
        driver = self.driver
        driver.implicitly_wait(3)
        driver.maximize_window()
        driver.get('https://demo-store.seleniumacademy.com/')

    def test_select_language(self):
        options = ['English', 'French', 'German'] #Listas con el nombre de las etiquetas
        act_options = []#Lista para almacenar las opciones al momento de revisar

        #Acceder a los elementos de la lista

        select_language = Select(self.driver.find_element_by_id('select-language'))

        #Validar si estan disponibles las opciones del dropdown

        self.assertEqual(3, len(select_language.options))#Esto nos permite ingresar a las opciones del dropdown

        for option in select_language.options:#Bucle para recorrer los elementos del dropdown
            act_options.append(option.text)#Agregar el texto

        #Comparacion de listas expuestas y activas

        self.assertListEqual(options, act_options)

        #Validar idioma por default

        self.assertEqual('English', select_language.first_selected_option.text) #Verificar que la palabra ingles esta por default

        #Seleccionar idioma del dropdown

        select_language.select_by_visible_text('German') #Seleccionar por el texto visible

        #Verificar que se cambio el idioma

        self.assertTrue('store=german' in self.driver.current_url)

        #Elegir un idioma mediante al indice

        select_language = Select(self.driver.find_element_by_id('select-language'))
        select_language.select_by_index(0)

    def tearDown(self):
        self.driver.implicitly_wait(3)
        self.driver.close()

if __name__ == "__main__":
    unittest.main(verbosity= 2)
```

## Manejar alert y pop-up

```python
import unittest
from selenium import webdriver

class CompareProducts(unittest.TestCase):

	def setUp(self):
		self.driver = webdriver.Chrome(executable_path = r'.\chromedriver.exe')
		driver = self.driver
		driver.implicitly_wait(30)
		driver.maximize_window()
		driver.get("http://demo-store.seleniumacademy.com/")
	
	def test_compare_products_removal_alert(self):
		driver = self.driver
		search_field = driver.find_element_by_name('q')
		#como buena práctica se recomienda limpiar los campos
		search_field.clear()


		search_field.send_keys('tee')
		search_field.submit()

		driver.find_element_by_class_name('link-compare').click()
		driver.find_element_by_link_text('Clear All').click()
		
		#creamos una variable para interactuar con el pop-up
		alert = driver.switch_to_alert()
		#vamos a extraer el texto que muestra
		alert_text = alert.text

		#vamos a verificar el texto de la alerta
		self.assertEqual('Are you sure you would like to remove all products from your comparison?', alert_text)
		
		alert.accept()

	def tearDown(self):
		self.driver.implicitly_wait(3)
		self.driver.close()

if __name__ == "__main__":
	unittest.main(verbosity = 2)
```

## Automatizar navegación

`automatic_navigation.py`

```python
import unittest
from selenium import webdriver
from time import sleep

class NavigationTest(unittest.TestCase):
  def setUp(self):
    self.driver = webdriver.Chrome(executable_path = r'./chromedriver')
    driver = self.driver
    driver.implicitly_wait(30)
    driver.maximize_window()
    driver.get('https://google.com')

  def test_browser_navigation(self):
    driver = self.driver

    search_field = driver.find_element_by_name('q')
    search_field.clear()
    search_field.send_keys('platzi')
    search_field.submit()

    driver.back()
    sleep(3)
    driver.forward()
    sleep(3)
    driver.refresh()
    sleep(3)

  def tearDown(self):
    self.driver.close()

if __name__ == "__main__":
  unittest.main(verbosity = 2)
```



Lista de **comandos para WebDriver** con su descripción, comparación y ejemplo:
https://www.techbeamers.com/important-selenium-webdriver-commands/

[web-driver-y-web-element.pdf](https://static.platzi.com/media/public/uploads/web-driver-y-web-element_8c0eb4df-888d-453f-ab39-79226c65c5e8.pdf)

# 5. incronizar pruebas

## Demora implícita y explícita

![DEMORAS.png](https://static.platzi.com/media/user_upload/DEMORAS-a913e27f-8d0d-497c-9a2e-956ecc048518.jpg)

`waits.py`

```python
import unittest
from selenium import webdriver

#Herramienta para seleccionar elementos de la web con sus selectores
from selenium.webdriver.common.by import By

#Herramienta para hacer uso de las expected conditions y esperas explicitas
from selenium.webdriver.support.ui import WebDriverWait

#Importar esperar explicitas
from selenium.webdriver.support import expected_conditions as EC


class ExplicitWaitTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(executable_path= r'./geckodriver.exe')
        driver = self.driver
        driver.implicitly_wait(10)
        driver.maximize_window()
        driver.get('https://demo-store.seleniumacademy.com/')

    def test_account_link(self): #Cuentas del sitio

        #Esperar 10 segundos hasta que se cumpla la funcion
        WebDriverWait(self.driver, 3).until(lambda s: s.find_element_by_id('select-language').get_attribute('length') == '3')

        #Hacer referencia al enlace donde estan las cuentas
        account = WebDriverWait(self.driver, 3).until(EC.visibility_of_element_located((By.LINK_TEXT, 'ACCOUNT')))
        account.click()


    def test_create_new_customer(self): #Creacion de nuevo usuario

        #Encontrar el elemento por el texto del enlace
        self.driver.find_element_by_link_text('ACCOUNT')

        #Hacer referencia a la cuenta
        my_account = WebDriverWait(self.driver, 3).until(EC.visibility_of_element_located((By.LINK_TEXT, 'My Account')))
        my_account.click()

        #Referencia a crear una cuenta
        create_account = WebDriverWait(self.driver, 2).until(EC.element_to_be_clickable((By.LINK_TEXT, 'CREATE AN ACCOUNT')))
        create_account.click()

        #Verificacion de estado de pagina web
        WebDriverWait(self.driver, 3).until(EC.title_contains('Create New Customer Account'))

    def tearDown(self):
        driver = self.driver
        driver.implicitly_wait(3)
        driver.close()

if __name__ == "__main__":
    unittest.main(verbosity= 2)
```

> **By** nos ayuda para hacer referencia a un elemento del sitio Web a través de sus selectores, NO para identificarlo, ES para **interactuar** de madera distinta a como lo hace WebDriver.
>
> **WebDriverWait** nos ayuda para hacer uso de las **ExpectedCondition** junto con las **Esperas Explicitas**

Existen dos tipos de demoras: implícita y explícita. La implícita, es aquella que le da un tiempo determinado a Selenium para encontrar por un elemento, Si este no se encuentra en ese tiempo, nos arroja un error. La explícita es aquella crea condiciones para que Selenium espere hasta que se cumplan. De esa forma podemos hacer que espere hasta que finalize algún proceso en la página.

Para utilizar WebcriverWait, tenemos que hacer referencia al navegador instanciado una coma y luego los segundos que se tomará y luego hacemos el uso de until de hasta que o hasta en español. para dar la condición. Tiene sentido, pues se puede leer como: Navegador espera 20 segundos hasta que la página tenga este estado.

[<img src="https://selenium-python.readthedocs.io/_static/logo.png" alt="Logo" style="zoom:25%;" />](https://selenium-python.readthedocs.io/waits.html)

## Condicionales esperadas

| **Expected Condition**                       | **Descripción**                                              | **Ejemplo**                                                  |
| -------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| element_to_be_clickable(locator)             | Espera a que el elemento sea visible y habilitado para hacer clic en el mismo | WebDriverWait(self.driver,10).until(expected_conditions.element_to_be_clickable(([By.NAME](http://by.name/),“accept_button”))) |
| element_to_be_selected(element)              | Espera a que un elemento sea seleccionado                    | subscription = self.driver.find_element_by_name(“terms”). WebDriverWait(self.driver, 10).until(expected_conditions.element_to_be_selected(terms))) |
| invisibility_of_element_located(locator)     | Espera a que un elemento no sea visible o no se encuentre presente en el DOM | WebDriverWait(self.driver,10).until(expected_conditions.invisibility_of_element_located(([By.ID](http://by.id/),“loading_banner”))) |
| presence_of_all_elements_located(locator)    | Espera a que por lo menos uno de los elementos que se indican coincida con los presentes en el sitio | WebDriverWait(self.driver,10).until(expected_conditions.presence_of_all_elements_located((By.CLASS_NAME,“input-text”))) |
| presence_of_element_located(locator)         | Espera a que un elemento sea visible se encuentre presente en el DOM | WebDriverWait(self.driver,10).until(expected_conditions.presence_of_element_located(([By.ID](http://by.id/),“search-bar”))) |
| text_to_be_present_in_element(locator,text_) | Espera a que un elemento con el texto indicado se encuentre presente | WebDriverWait(self.driver,10).until(expected_conditions.text_to_be_present_in_element(([By.ID](http://by.id/),“seleorder”),“high”)) |
| title_contains(title)                        | Espera a que la página contenga en el título exactamente como es indicado | WebDriverWait(self.driver, 10).until(expected_conditions.title_contains(“Welcome”)) |
| title_is(title)                              | Espera a que la página tenga un título idéntico a como es indicado | WebDriverWait(self.driver, 10).until(expected_conditions.title_is(“Welcome to Platzi”)) |
| visibility_of(element)                       | Espera a que el elemento indicado esté en el DOM, sea visible, su alto y ancho sean mayores a cero | first_name = self.driver.find_element_by_id(“firstname”) WebDriverWait(self.driver, 10).until(expected_conditions.visibility_of(first_name)) |
| visibility_of_element_located(locator)       | Espera a que el elemento indicado por su selector esté en el DOM, sea visible y que su alto y ancho sean mayores a cero | WebDriverWait(self.driver,10).until(expected_conditions.visibility_of_element_located(([By.ID](http://by.id/),“firstname”))) |

# 6. Retos

## Agregar y eliminar elementos

```python
import unittest
from selenium import webdriver
from time import sleep

class AddRemoveElements_Reto(unittest.TestCase):

    def setUp(self):

        self.driver = webdriver.Chrome(executable_path = './chromedriver')
        driver = self.driver
        driver.get('https://the-internet.herokuapp.com/')
        driver.find_element_by_link_text('Add/Remove Elements').click()

    
    def test_add_remove(self):
        driver = self.driver

        elements_added = int(input('Cuantos elementos desea agregar?: '))

        add_button = driver.find_element_by_xpath('//*[@id="content"]/div/button')

        sleep(3)

        for i in range(elements_added):
            add_button.click()

        elements_removed = int(input('cuantos elementos desea remover?: '))

        while elements_removed > elements_added:
        
            for i in range(elements_removed):

                    print("Intentas remover mas elementos de los que ingresaste")

                    if elements_removed > elements_added:

                        elements_removed = int(input('cuantos elementos desea remover?: '))
                        
                        delete_button = driver.find_element_by_xpath('//*[@id="elements"]/button[1]')
                        delete_button.click()
                        break   

        sleep(3)                    
        total_elements = elements_added - elements_removed

        if total_elements == 1:

            print(f'Removiste {elements_removed} elementos, queda {total_elements} elemento')

        elif total_elements > 1 or total_elements < 1:
            
            print(f'Removiste {elements_removed}, quedan {total_elements} elementos')  


    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main(verbosity = 2)
```

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)The Internethttp://the-internet.herokuapp.com/](http://the-internet.herokuapp.com/)

## Elementos dinámicos

`dynamic_elements.py`

```python
import unittest
from selenium import webdriver
from time import sleep

class DynamicElements(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path = r'./chromedriver')
    driver = self.driver
    driver = self.driver
    driver.get("http://the-internet.herokuapp.com")
    driver.find_element_by_link_text("Disappearing Elements").click()

  def test_name_elements(self):
    driver = self.driver

    options = []
    menu = 5
    tries = 1

    while len(options) < 5:
      options.clear()

      for i in range(menu):
        try:
          option_name = driver.find_element_by_xpath(f"/html/body/div[2]/div/div/ul/li[{i + 1}]/a")
          options.append(option_name.text)
          print(options)
        except:
          print(f"Options number {i + 1} is NOT FOUND")
          tries += 1
          driver.refresh()

    print(f"Finished in {tries} tries")

  def tearDown(self):
    self.driver.close()

if __name__ == "__main__":
  unittest.main()
```

## Controles dinámicos

`dynamic_control.py`

```python
import unittest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from time import sleep


class DynamicElements(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path=r'./chromedriver')
    driver = self.driver
    driver = self.driver
    driver.get("http://the-internet.herokuapp.com")
    driver.find_element_by_link_text("Disappearing Elements").click()

  def test_dynamic_controls(self):
    driver = self.driver

    checkbox = driver.find_element_by_css_selector("#checkbox > input[type=checkbox]")
    checkbox.click()

    remove_add_button = driver.find_elements_by_css_selector("#checkbox-example > button")
    remove_add_button.click()

    WebDriverWait(driver, 15).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#checkbox-example > button")))
    remove_add_button.click()

    enable_disable_button = driver.find_element_by_css_selector("#input-example > button")
    enable_disable_button.click()

    WebDriverWait(driver, 15).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#input-example > button")))

    text_area = driver.find_element_by_css_selector("#input-example > input[type=text]")
    text_area.send_keys('Platzi')

    enable_disable_button.click()

  def tearDown(self):
    self.driver.close()

if __name__ == "__main__":
  unittest.main()
```

## Typos

```python
import unittest
from selenium import webdriver

class Typos(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path=r'./chromedriver')
    driver = self.driver
    driver = self.driver
    driver.get("http://the-internet.herokuapp.com")
    driver.find_element_by_link_text("Typos").click()

  def test_find_typo(self):
    driver = self.driver

    paragraph_to_check = driver.find_element_by_css_selector("#content > div > p:nth-child(3)")
    text_to_check = paragraph_to_check.text
    print(text_to_check)

    tries = 1
    found = False
    correct_text = "Sometimes you'll see a typo, other times you won't."

    while text_to_check != correct_text:
      paragraph_to_check = driver.find_element_by_css_selector("#content > div > p:nth-child(3)")
      text_to_check = paragraph_to_check.text
      driver.refresh()

    while not found:
      if text_to_check == correct_text:
        tries += 1
        driver.refresh()
        found = True

    self.assertEqual(found, True)

    print(f"It took  {tries} tries to find the typo")

  def tearDown(self):
    self.driver.close()


if __name__ == "__main__":
  unittest.main()
```

## Ordenar tablas

`table.py`

```python
import unittest
from selenium import webdriver
from time import sleep

class Tables(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path=r'./chromedriver')
    driver = self.driver
    driver = self.driver
    driver.get("http://the-internet.herokuapp.com")
    driver.find_element_by_link_text("Typos").click()

  def test_sort_tables(self):
    driver = self.driver

    table_data = [[] for i in range(5)]
    print(table_data)

    for i in range(5):
      header = driver.find_element_by_xpath(f'//*[@id="table1"]/thead/tr/th[{i + 1}]/span')
      table_data[i].append(header.text)

      for j in range(4):
        row_data = driver.find_element_by_xpath(f'//*[@id="table1"]/tbody/tr[{j + 1}]/td[{j + 1}]')
        table_data[i].append(row_data.text)

    print(table_data)


  def tearDown(self):
    self.driver.close()

if __name__ == "__main__":
  unittest.main()
```

# 7. Metodologías de Trabajo

## Data Driven Testing (DDT)

**TDD:** Creas Software y durante el desarrollo, vas aprobando los Tests que ya están definidos (creados).

**DDT:** Creas Tests para que el Software que ya esta escrito las apruebe.

install `ddt`

```sh
pip install ddt
```

`search_ddt.py`

```python
import unittest
from ddt import ddt, data, unpack
from selenium import webdriver

@ddt
class SearchDDT(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path=r'./chromedriver')
    driver = self.driver
    driver.implicitly_wait(30)
    driver.maximize_window()
    driver.get("http://demo-store.seleniumacademy.com/")

  @data(('dress', 6), ('music', 5))
  @unpack

  def test_search_ddt(self, search_value, expected_count):
    driver = self.driver
    search_field = driver.find_element_by_name('q')
    search_field.clear()
    search_field.send_keys(search_value)
    search_field.submit()

    products = driver.find_elements_by_xpath('//h2[@class="product-name"]/a')
    print(f'Found {len(products)} products')

    for product in products:
      print(product.text)

    self.assertEqual(expected_count, len(products))


  def tearDown(self):
    self.driver.implicitly_wait(3)
    self.driver.close()


if __name__ == '__main__':
    unittest.main(verbosity=2)
```

`search_csv_ddt.py`

```python
import csv, unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
from ddt import ddt, data, unpack

def get_data(file_name):
  rows = []
  data_file = open(file_name, 'r')
  reader = csv.reader(data_file)
  next(reader, None)

  for row in reader:
    rows.append(row)
  return rows

@ddt
class SearchCsvDDT(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome(executable_path = r'./Chromedriver')
    driver = self.driver
    driver.implicitly_wait(30)
    driver.maximize_window()
    driver.get("http://demo-store.seleniumacademy.com/")

  @data(*get_data('testdata.csv'))
  @unpack

  def test_search_ddt(self, search_value, expected_count):
    driver = self.driver
    search_field = driver.find_element_by_name('q')
    search_field.clear()
    search_field.send_keys(search_value)
    search_field.submit()

    products = driver.find_elements_by_xpath('//h2[@class="product-name"]/a')

    expected_count = int(expected_count)

    if expected_count > 0:
      self.assertEqual(expected_count, len(products))
    else:
      message = driver.find_element_by_class_name('note-msg')
      self.assertEqual('Your search returns no results.', message)

    print(f'Found {len(products)} products')

  def tearDown(self):
    self.driver.close()


if __name__ == '__main__':
  unittest.main(verbosity=2, testRunner=HTMLTestRunner(output='reports', report_name='hello-world-report'))

```

## Page Object Model (POM)

**Page Object Model** es un patrón de diseño utilizado en Testing, el cual, tiene **beneficios para la elaboración de Tests.**

**Funcionamiento:**

- En vez de tener nuestros **Test en un solo archivo,** los tendremos **abstraídos en una nueva capa llamada Pages,** donde tendremos los Tests en archivos independientes, haciendo referencia al sitio donde se aplica (Home, Login, etc).

A los archivos independientes se les agregará un nuevo archivo que es el **Page Object,** el cual se encargará de tomar los Tests que se están realizando y **validarlas contra los Tests Cases.**

`google_page.py`

```python
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class GooglePage(object):
  def __init__(self, driver):
    self._driver = driver
    self._url = 'https://google.com'
    self.search_locator = 'q'

  @property
  def is_loaded(self):
    WebDriverWait(self._driver, 10).until(EC.presence_of_element_located((By.NAME, 'q')))
    return True

  @property
  def keyword(self):
    input_field = self._driver.find_element_by_name('q')
    return input_field.get_attribute('value')

  def open(self):
    self._driver.get(self._url)

  def type_search(self, keyword):
    input_field = self._driver.find_element_by_name('q')
    input_field.send_keys(keyword)

  def click_submit(self):
    input_field = self._driver.find_element_by_name('q')
    input_field.submit()

  def search(self, keyword):
    self.type_search(keyword)
    self.click_submit

```

`test_google.py`

```python
import unittest
from selenium import webdriver
from google_page import GooglePage

class GoogleTest(unittest.TestCase):

  @classmethod
  def setUpClass(cls):
    cls.driver = webdriver.Chrome(executable_path = r'../chromedriver')

  def test_search(self):
    google = GooglePage(self.driver)
    google.open()
    google.search('Platzi')

    self.assertEqual('Platzi', google.keyword)

  @classmethod
  def tearDownClass(cls):
    cls.driver.close()

if __name__ == "__main__":
  unittest.main(verbosity = 2)
```

# 8. Cierre del curso

## Realizar una prueba técnica

**Consideraciones**

- Practicar en sitios complejos
- Preguntar acerca de las expectativas
- Define paso a paso el flujo
- Piensa como usuario final
- Programa como desarrollador

**Flujo en Mercao Libre**

1. Ingresa a mercadolibre.com
2. Seleccionar 'Colombia' com pais.
3. Buscar el termino "playstation 4"
4. Filtar por condiciones "Nuevos"
5. Filtrar por ubicacion "Bogota"
6. Ordenar de mayor a menor precio
7. Obtener el nombre y precio de los primeros 5 articulos

`mercadolibre.py`

```python
import unittest
from selenium import webdriver
from time import sleep


class TestingMercadoLibre(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r'./chromedriver')
        driver = self.driver
        driver.get('https://www.mercadolibre.com')
        driver.maximize_window()

    def test_search_ps4(self):
        driver = self.driver

        contry = driver.find_element_by_id('CO')
        contry.click()

        search_field = driver.find_element_by_name('as_word')
        search_field.click()
        search_field.clear()
        search_field.send_keys('playstation 4')
        search_field.submit()
        sleep(3)

        location = driver.find_element_by_partial_link_text('Bogotá D.C.')
        # location = driver.find_element_by_xpath("//div/div/aside/section[2]/dl[9]/dd[1]/a")
        location.click()
        sleep(3)

        condition = driver.find_element_by_partial_link_text('Nuevo')
        condition.click()
        sleep(3)

        order_menu = driver.find_element_by_class_name('ui-dropdown__link')
        order_menu.click()
        higher_price = driver.find_element_by_css_selector('#root-app > div > div > section > div.ui-search-view-options__container > div > div > div > div.ui-search-view-options__title')
        higher_price.click()
        sleep(3)

        articles = []
        prices = []

        for i in range(5):
          article_name = driver.find_element_by_xpath(f'/html/body/main/div/div/section/ol/li[{i + 1}]/div/div/div[2]/div[1]/a/h2').text
          articles.append(article_name)
          article_price = driver.find_element_by_xpath(f'/html/body/main/div/div/section/ol/li[{i + 1}]/div/div/div[2]/div[2]/div[1]/div[1]/div/div/span[1]/span[2]/span[2]')
          prices.append(article_price)

        print(articles, prices)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()

```




## Conclusiones

