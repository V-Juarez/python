<h1>Profesional de Python</h1>

<h3>Facundo García Martoni</h3>

<h2>Tabla de Contenido</h2>

- [1. Introducción](#1-introducción)
  - [¿Qué necesitas saber para tomar el curso?](#qué-necesitas-saber-para-tomar-el-curso)
  - [¿Cómo funciona Python?](#cómo-funciona-python)
  - [Cómo organizar las carpetas de tus proyectos](#cómo-organizar-las-carpetas-de-tus-proyectos)
    - [`tree`.](#tree)
    - [Como Organizar las Carpetas de tus Proyectos](#como-organizar-las-carpetas-de-tus-proyectos)
    - [How to organize your proyect folders](#how-to-organize-your-proyect-folders)
- [2. Static Typing](#2-static-typing)
  - [¿Qué son los tipados?](#qué-son-los-tipados)
      - [¿Qué son los tipados?](#qué-son-los-tipados-1)
    - [What are types?](#what-are-types)
  - [Tipado estático en Python](#tipado-estático-en-python)
      - [Modulo `mypy`](#modulo-mypy)
  - [Practicando el tipado estático](#practicando-el-tipado-estático)
- [3. Conceptos avanzados de funciones](#3-conceptos-avanzados-de-funciones)
  - [Scope: alcance de las variables](#scope-alcance-de-las-variables)
      - [Local Scope](#local-scope)
      - [Global Scope](#global-scope)
      - [Scope: alcance de las variables](#scope-alcance-de-las-variables-1)
  - [Closures](#closures)
      - [Closures](#closures-1)
  - [Programando closures](#programando-closures)
  - [Decoradores](#decoradores)
  - [Programando decoradores](#programando-decoradores)
- [4. Estructuras de datos avanzadas](#4-estructuras-de-datos-avanzadas)
  - [Iteradores](#iteradores)
  - [La sucesión de Fibonacci](#la-sucesión-de-fibonacci)
      - [La sucesión de Fibonacci](#la-sucesión-de-fibonacci-1)
  - [Generadores](#generadores)
      - [Generator Expression](#generator-expression)
  - [Sets](#sets)
  - [Operaciones con sets](#operaciones-con-sets)
      - [Unión](#unión)
      - [Intersección](#intersección)
      - [Diferencia](#diferencia)
      - [Diferencia Simétrica](#diferencia-simétrica)
    - [Métodos Set](#métodos-set)
  - [Eliminando los repetidos de una lista](#eliminando-los-repetidos-de-una-lista)
- [5. Bonus](#5-bonus)
  - [Manejo de fechas](#manejo-de-fechas)
  - [Time zones](#time-zones)
- [6. Conclusión](#6-conclusión)
  - [Completaste la trilogía. ¿Cómo seguir?](#completaste-la-trilogía-cómo-seguir)

# 1. Introducción

## ¿Qué necesitas saber para tomar el curso?

[Slider-python-Profesional.pdf](https://drive.google.com/file/d/1MdGNMk-srifCbhYvq6E6QsI2Kgadlvqn/view?usp=sharing)

## ¿Cómo funciona Python?

<img src="https://i.ibb.co/dsvw1mw/compilate.jpg" alt="compilate" border="0">

- Tanto **compiladores** como **interpretadores** son programas que convierten el código que escribes a lenguaje de máquina.
  **Compilado:** Aquel lenguaje que tiene que ser traducido al código de máquina para producir un programa ejecutable
- **Interpretado:** es un lenguaje de programación para el que la mayoría de sus implementaciones ejecuta las instrucciones directamente, sin una previa compilación del programa a instrucciones en lenguaje máquina.
- **Garbage collector:** Es una sección especial de python que se encarga de tomar los objetos y las variables que no están en uso y eliminarlas.
- **¿Qué es la carpeta \**pycache\**?** Dentro de la carpeta tenemos el bytecode que es el código intermedio que crea python al ser un lenguaje interpretado para que pueda ser leido por la máquina virtual, la ventaja es que funiona como una especie de recuperacion del código que ya hemos trabajado, para que la proxima vez que ejecutes el programa se ejecutará más rápido porque no tiene que convertirse a bytecode de nuevo.

El recolector de basura (Garbage Collector) sirve como administrador de memoria automático. El recolector de basura administra la asignación y liberación de memoria para una aplicación.

<img src="https://i.ibb.co/mNYyxDY/Garbage-collection.gif" alt="Garbage-collection" border="0">



<img src="https://i.ibb.co/D7Y5fKb/tiposdelenguaje.jpg" alt="tiposdelenguaje" border="0">

Tambien podemos verlo de esta forma:

1. Los lenguajes compilados convierten el código a binario que es el que lee la computadora.
2. Los interpretados requieren de un programa que lee las instrucciones en tiempo real y las ejecuta, por lo que el programa interpreta el código escrito y lo traduce en lenguaje de máquina en tiempo real. Esto también explicaría porque en los notebook escritos en collab o jupyter podemos ejecutar nuestro código de python por partes.

## Cómo organizar las carpetas de tus proyectos

📁Un **módulo** es cualquier archivo de Python. Generalmente, contiene código que puedes reutilizar.

🗄 Un paquete es un conjunto de módulos. Siempre posee el archivo **`__init__.py`**.

Is pronounced **“dunder init”**: dunder is short for “double-underscore”.

Una ejemplo de organizar los archivos de 🐍Python es de la siguiente manera.

<img src="https://i.ibb.co/SRWM6ys/paquettes.jpg" alt="paquettes" border="0">

### `tree`.

Se vería algo así:

![img](https://i.imgur.com/qCVtw4H.png)

Yo pongo `tree -I venv` para ignorar la carpeta venv que esta llena de cosas. Si no lo pongo verás todos los directorios de tu proyecto.

![img](https://i.imgur.com/H9wQKS3.png)

### Como Organizar las Carpetas de tus Proyectos

Las carpetas se organizan por Paquetes y Módulos, estos últimos son cualquier archivo de Python. Generalmente, contiene código que puedes reutilizar

Un Paquete es una carpeta que contiene módulos. Siempre posee el archivo **init**.py

### How to organize your proyect folders

To make this thema more simple Facundo introduces two concepts:

- modules: A module is any python file, usually this is reusable in other python files, for example a function created to reuse.
- packages: A package is a folder that contain many modules. In this folder, always there is _*init*.py, it’s necessary to the package.

```bash
package:
 - module_1
 - module_2
 - module_3
 - module_n
 all modules are relationized to the __init__
```

Example of directory project:

- README
- .gitignore
- venve
- package:
  - **init**.py
  - module_1.py
  - module_2.py
- README: an explanation over our proyect, normally use a .md format.
- .gitignore: file to ignore other files at the time to run.
- venve: this is the folder with the envioment variables.

Frameworks: Set of rules to the app run. They have them folders…

# 2. Static Typing

## ¿Qué son los tipados?

💻 Los tipados es una clasificación de los lenguajes de programación, tenemos cuatro tipos:

- Estático
- Dinámico
- Débil
- Fuerte

El tipado del lenguaje depende de cómo trata a los tipos de datos.

El tipado estático es el que levanta un error en el tiempo de compilación, ejemplo en JAVA:

```java
String str = "Hello" // Variable tipo String
str = 5 // ERROR: no se puede convertir un tipo de dato en otro de esta forma.
```

El tipado dinámico levantan el error en tiempo de ejecución, ejemplo en Python:

```python
str = "Hello" # Variable tipo String
str = 5 # La variable ahora es de tipo Entero, no hay error

## TIPADO FUERTE
x = 1
y = "2"
z = x + y # ERROR: no podemos hacer estas operaciones con tipos de datos distintos entre sí
```

El tipado débil es el que hace un cambio en un tipo de dato para poder operar con el, como lo hace JavaScript y PHP.

🐍 Python es un lenguaje de tipado 👾 Dinámico y 💪 Fuerte.

**Estático** →→ Detectan los errores en tiempo de compilación. No se ejecuta hasta corregir. Por ej, *Java*

**Dinámico** →→ Detectan el error en tiempo de ejecución. Nos dice el error cuando llega a la línea del código. Por ej, *Python*

**Strong** →→ Más severidad con los tipos de datos. Sumar un número + una letra arrojará error.

**Weak** →→ Menos severidad con los tipos de datos. Si quiero sumar número y letra, las concatenaría como strings. Castea tipos de datos automáticamente. Por ej, *PHP*

El tipado del lenguaje depende de cómo trata a los tipos de datos o datos primitivos (Números, Arreglos, Booleanos, Strings).

#### ¿Qué son los tipados?

- Estático -> Levantan los errores de tipo en tiempo de compilación - El programa no se ejecuta hasta que el error se resuelva
- Dinámico -> Levantan el error el tiempo de ejecución - Cuando llegue al error, lo va a indicar
- Débil
- Fuerte

Un lenguaje va a tener un tipado diferente dependiendo de cómo trate a los tipos.

- Strong & Dinamic 💪💫 : Python, Ruby, Erlang
- Strong & Static 💪🗻: Java, C#, Scala
- Weak & Dynamic 😫💫 : JavaScript, PHP, Perl
- Weak & Static 😫🗻 : C, C++

Estático:

```java
// java
String str = "hello";
str = 5; // Error
```

Dinámico:

```python
# python
str = "Hello"
str = 5 # No hay problema :)
# python
x = 1
y = "2"
z = x + y # Error
// javascript
const x = 1
const y = "2"
const z = x + y // "12" - JS es raro 😅
<?php
$str = 5 + "5"; //10 - PHP es raro 😅 (hace lo contrario a JS)
?>
```

Nota: El tipado dinámico es **peligroso**.

### What are types?

A type is a classification of language. It refers how each language maganes the data type,
like we saw in previous courses, integer, string, booleans.

Bellow the list of types:

- Type static: erros appear at compilation time.
- Type dynamic: erros appear at execution time.
- Type strong: the language is severe with operations. int + str is not possible.
- Type Weak: the language allow operations of differtent data type. int + str is done as string or integer.

Laguages Classification:

|     Types      |  Languages   |
| :------------: | :----------: |
| Strong-Static  |   Java, C#   |
|  Weak-Static   |    C, C++    |
|  Weak-Dynamic  |   php, JS    |
| Strong-Dynamic | Python, Ruby |

![img](https://www.google.com/s2/favicons?domain=https://cdn.sstatic.net/Sites/stackoverflow/Img/favicon.ico?v=ec617d715196)[terminology - Static/Dynamic vs Strong/Weak - Stack Overflow](https://stackoverflow.com/questions/2351190/static-dynamic-vs-strong-weak)

## Tipado estático en Python

[Documentación oficial del tipado estático en Python](https://docs.python.org/3/library/typing.html)

El tipado estático nos hará evitar errores de tipado antes de que el programa se ejecute.

```python
a: int = 5
print(a) # 5

b: str = 'Hola'
print(b) # Hola

c: bool = True
print(c) # True
```

Esta sintaxís está disponible desde la versión 3.6 de Python.

```python
def suma(a: int, b: int) -> int:
  return a + b

print(suma(1,2)) # 3
def suma(a: int, b: int) -> int:
  return a + b

print(suma('1','2')) # 12 😅
```

Usando tipado en estructuras de datos. Desde la versión 3.6 debemos importar librerias.

```python
from typing import Dict, List

positives: List[int] = [1,2,3,4,5]

users: Dict[str, int] = {
  'argentina': 1,
  'mexico': 34,
  'colombia': 45,
}

countries: List[Dict[str,str]] = [
  {
    'name': 'Argentina',
    'people': '450000', # Cuatrocientos cincuenta mil
  },
  {
    'name': 'México',
    'people': '90000000', # Noventa millones
  },
  {
    'name': 'Colombia',
    'people': '99999999999', #novecientos noventa y nueve mil millones novecientos mil novecientos noventa y nueve
  }
]
from typing import Tuple

numbers: Tuple[int, float, int] = (1, 0.5, 1)
from typing import Tuple, Dict, List

CoordinatesType = List[Dict[str, Tuple[int, int]]]

#Una variable que es de tipo CoordinatesType 🤯
coordinates: CoordinatesType = [
  {
    'coord1': (1,2),
    'coord2': (3,5),
  },
  {
    'coord1': (0,1),
    'coord2': (2,5),
  },
]
```

Para hacer que Python sea de tipado estático es necesario agregar algo de sintaxis adicional a lo aprendido, además, esta característica solo se puede aplicar a partir de la versión 3.6.

```python
# De esta manera se declara una variable, se colocan los dos puntos (:), el tipo de dato y para finalizar se usa el signo igual para asignar el valor a la variable.

<variable> : <tipo_de_dato> = <valor_asignado>

a: int = 5
print(a)

b: str = "Hola"
print(b)

c: bool = True
print(c)
```

Del mismo modo se puede usar esta metodología de tipado en Python a funciones adicionando el signo menos a continuación del signo mayor que para determinar el tipo de dato. Ejemplo:

```python
def <nombre_func> ( <parametro1> : <tipo_de_dato>, <parametro2> : <tipo_de_dato> ) ->  <tipo_de_dato> :
	pass

def suma(a: int, b: int) -> int :
	return a + b

print(suma(1,2))

# 3
```

Existe una librería de fabrica que viene preinstalada con Python que se llama **typing,** que es de gran utilidad para trabajar con tipado con estructuras de datos entre la versión **3.6** y **3.9**, entonces:


```python
from typing import Dict, List

positives: List [int] = [1,2,3,4,5]

users: Dict [str, int] = {
	"argentina": 1.
	"mexico": 34,
	"colombia": 45,
}

countries: List[Dict[str, str]] = [
	{
		"name" : "Argentina",
		"people" : "45000",
	},
	{
		"name" : "México",
		"people" : "9000000",
	},
	{
		"name" : "Colombia",
		"people" : "99999999999",
	}
]
from typing import Tuple, Dict, List

CoordinatesType = List[Dict[str, Tuple[int, int]]]

coordinates: CoordinatesType = [
	{
		"coord1": (1,2),
		"coord2": (3,5)
	},
	{
		"coord1": (0,1),
		"coord2": (2,5)
	}
]
```

#### Modulo `mypy`

El modulo mypy se complementa con el modulo typing ya que permitirá mostrar los errores de tipado debil en Python.

Pydantic que es un validador de tipos. [pydantic](https://pydantic-docs.helpmanual.io/)

![img](https://www.google.com/s2/favicons?domain=https://docs.python.org/es/3/library/typing.html../_static/py.png)[typing — Soporte para type hints — documentación de Python - 3.9.6](https://docs.python.org/es/3/library/typing.html)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Getting started — Mypy 0.910 documentation](https://mypy.readthedocs.io/en/stable/getting_started.html)

## Practicando el tipado estático

- Following the tips of intermediate python course, from terminal:

1. mkdir new_folder
2. git init
3. py -m venv venv
4. touch .gitignore
   type in .gitignore file

```bash
# Ignore the enviroment files when you push to github.
venv
```

1. avenv (alias to activate venv: `.\venv\Scripts\activate`)

   Para activar venv usando Linux

   ```bash
   source venv/bin/activate
   ```

2. pip install mypy

3. pip list (to check mypy)

4. touch palindrome-py

5. code palindrome-py

6. Make your code!!!

7. Error.

```bash
mypy palindrome-py --check-untyped-defs
```

**Reto**

```python
def primo(numero: int) -> bool:
    contador = 0
    for i in range(1, numero + 1):
        if i == 1 or i == numero:
            continue
        if numero % i == 0:
            contador += 1
        
    return contador == 0

if __name__ == '__main__':
    print(primo(100))
```

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Getting started — Mypy 0.910 documentation](https://mypy.readthedocs.io/en/stable/getting_started.html)

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - platzi/curso-profesional-python at static-typing](https://github.com/platzi/curso-profesional-python/tree/static-typing)

# 3. Conceptos avanzados de funciones

## Scope: alcance de las variables

El scope es el alcance que tienen las variables. Depende de donde declares o inicialices una variable para saber si tienes acceso. **Regla de oro:**

> *una variable solo esta disponible dentro de la region donde fue creada*

#### Local Scope

Es la región que corresponde el ámbito de una función, donde podremos tener una o mas variables, las variables van a ser accesibles únicamente en esta region y no serán visibles para otras regiones

#### Global Scope

Al escribir una o mas variables en esta region, estas podrán ser accesibles desde cualquier parte del código.

![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-5d8878b2-048d-4017-a0de-a582e3f494d5.jpg)

#### Scope: alcance de las variables

Una variable solo está disponible dentro de la región donde fue creada.

```python
# Local Scope

def my_func():
  y = 5 #La variable solo está disponible en esta función
  print(y)

my_func() # 5
```

------

```python
# Global Scope
y = 5

def my_func():
  print(y)

def my_other_func():
  print(y)

my_func() # 5
my_other_func() # 5
```

------

```python
z = 5 # global

def my_func():
  z = 3 # z local
  print(z)

my_func() # 3 - local

print(z) # 5 - global
```

------

```python
z = 5

def my_func():
  z = 3

  def my_other_func():
    z = 2
    print(z)

  my_other_func() # 2

  print(z) # 3

my_func()

print(z) # 5
```

## Closures

#### Closures

<h5>Nested Functions - Funciones anidadas</h5>

```python
def main():
  a = 1

  def nested():
    print(a)

  nested() # 1

main()
```

------

closure:

```python
def main():
  a = 1

  def nested():
    print(a)

  return nested

my_func = main()
my_func() # 1
```

Quiere decir que una variable que está en un scope superior, es recordada por una función de scope inferior.

------

```python
def main():
  a = 1

  def nested():
    print(a)

  return nested

my_func = main()
my_func() # 1

del(main) # borramos la función

my_func() # 1 🤯 debido a la nested function
```

Reglas para encontrar un closure:

- Debemos tener una nested function.
- La nested function debe referenciar un valor de un scope superior.
- La función que envuelve a la nested function debe retornarla también.

```python
def make_multiplier(x):

  def multiplier(n):
    return x * n

  return multiplier

times10 = make_multiplier(10)
times4 = make_multiplier(4)

print(times10(3)) # => 10 * 3 = 30
print(times4(5)) # => 4 * 5 = 20
print(times10(times4(2))) # => 4 * 2 * 10 = 80
```

¿Dónde aparecen los closures?

1. Clase con solo 1 método
2. Cuando trabajamos con decoradores

**Reglas para encontrar un closure**

1. Debemos tener una nested function
2. La nested function debe referenciar un valor de un scope superior
3. La función que envuelve la nested debe retornarla también

cuando tenemos una clase que tiene solo un método
cuando trabajamos con decoradores

Con este tipo de estructuras podemos crear funciones personalizadas. Se les suele usar o encontrar cuando tenemos, por ejemplo, clases cortas con un solo método (para hacerla mas elegante) o cuando trabajamos con decoradores.

Ejemplo:

```python
def main():
	
	a = 1

	def nested():
		print(a)
	return nested

myfunc = main()
myfunc() #1
```

En el ejemplo anterior incluso se puede borrar la función **main** ( `del(main)` ) y la variable **myfunc** aún recordará la variable a que se encuentra en la función **main**, esto porque es una variable de un *scope* superior.
En el siguiente ejemplo podemos hacer una función *personalizada*:

```python
def make_multiplier(x):

    def multiplier(n):
        return x * n
    return multiplier

times10 = make_multiplier(10)
times4 = make_multiplier(4)

print(times10(3))         #30
print(times4(5))          #20
print(times10(times4(2))) #80
```

## Programando closures

**Code class**

```python
# HOla 3 -> HolaHolaHola
# facundo 2 -> facundofacundo
# Platzi 4 -> PlatziPlatziPlatziPlatzi

def make_repeater_of(n):
  def repeater(string):
    assert type(string) == str, "Solo puedes utilizar cadenas"
    return string * n
  return repeater


def run():
  repeat_5 = make_repeater_of(5)
  print(repeat_5("Hola"))
  repeat_10 = make_repeater_of(10)
  print(repeat_10("Platzi"))


if __name__ == '__main__':
  run()
```

**Reto**

```python
def make_division(n):

    def division_by(x):

        return x/n 
    
    return division_by


def run():
    first_division = make_division(2)
    print(first_division(10))


if __name__ == '__main__':
    run()
```

## Decoradores

La funcion tiene que llamarse en la nested para que la funcion decorador pueda incluirla. Sin la nested tendriamos que llamar a la funcion decorador e incluirle la funcion hola. Con este sistema solo creamos el decorador con nested y lo colocamos como decorador en la funcion que queremos decorar

La sintaxis sería la siguiente:

```python
def <funcion_decorador> (<funcion>):

    def <funcion_interna> ():

        <código de la funcion_interna>

    return <funcion_interna>
```

No obstante, dejó el código de ejemplo que usó para explicar este tema por si a alguien le interesa:

```python
def funcion_decoradora(funcion_parametro):

    def funcion_interior():

        #Acciones adicionales que decoran

        print("Vamos a realizar un cálculo: ")

        funcion_parametro()

        #Acciones adicionales que decoran

        print("Hemos terminado el cálculo")

    return funcion_interior


# Para llamar la función decoradora se usa el carácter "@" y a continuación el nombre de esta.
# Python entiende que la función adyacente hacia abajo es la función a decorar y la toma como parametro.

@funcion_decoradora
def suma():
    print(15+20)

suma()
```

El resultado en consola sería el siguiente:

```bash
>>> Vamos a realizar un cálculo: 
>>> 35
>>> Hemos terminado el cálculo
```

**Apuntes de la clase**
**Decoradores**

> Es un closure especial, es una función que recibe parametros de otra función, le añade cosas y retorna una función diferente

```python
def decorador(func):
    def envoltura():
        print('esto se añade a mi función original')
        func()
    return envoltura

def saludo():
    print('¡Hola!')
    
#llamado sencillo
saludo()

#llamado decorado
saludo = decorador(saludo)
saludo()

#output
¡Hola!
esto se añade a mi función original
¡Hola!
```

Una forma más pythonica es con ‘Azúcar Sintáctica’/Sugar Syntax, ‘enbellece’ el código para comprenderlo mejor

```python
def decorador(func):
    def envoltura():
        print('esto se añade a mi función original')
        func()
    return envoltura

@decorador
def saludo():
    print('¡Hola!')
    
saludo()

#output
esto se añade a mi función original
¡Hola!
```

Otro ejemplo:

```python
def mayusculas(func):
def envoltura(texto):
return func(texto).upper()
return envoltura

@mayusculas
def mensaje(nombre):
return f'{nombre}, recibiste un mensaje'

print(mensaje('Felipe'))

#output
FELIPE, RECIBISTE UN MENSAJE
```

**código**:

```python
def mayusculas(func):
    def wrapper(texto):
        return func(texto).upper()
    return wrapper

@mayusculas
def mensaje(nombre):
    return f'{nombre}, recibiste un mensaje'

print(mensaje('Cesar'))
```

## Programando decoradores

decoradores que reciben parámetros, para esto solo tiene que anidar una función más para que esta reciban los parámetros del decorador y la siguiente es la que recibe la función a la que decora.

![decorator.png](https://static.platzi.com/media/user_upload/decorator-55b2d821-f440-4063-904c-d075abe19c23.jpg)

# 4. Estructuras de datos avanzadas

## Iteradores

Antes de entender qué son los iteradores, primero debemos entender a los iterables.

Son todos aquellos objetos que podemos recorrer en un ciclo. Son aquellas estructuras de datos divisibles en elementos únicos que yo puedo recorrer en un ciclo.

Pero en Python las cosas no son así. Los iterables se convierten en iteradores.

Ejemplo:

```python
# Creando un iterador

my_list = [1,2,3,4,5]
my_iter = iter(my_list)

# Iterando un iterador

print(next(my_iter))

# Cuando no quedan datos, la excepción StopIteration es elevada
```

------

```python
# Creando un iterador

my_list = [1,2,3,4,5]
my_iter = iter(my_list)

# Iterando un iterador

while True: #ciclo infinito
  try:
    element = next(my_iter)
    print(element)
  except StopIteration:
    break
```

**Momento impactante:** El ciclo “for” dentro de Python, no existe. Es un while con StopIteration. 🤯🤯🤯

```python
my_list = [1,2,3,4,5]

for element in my_list:
  print(element)
```

------

`evenNumbers.py`:

```python
class EvenNumbers:
  """Clase que implementa un iterador de todos los números pares,
  o los números pares hasta un máximo
  """

  #* Constructor de la clase
  def __init__(self, max = None): #self hace referencia al objeto futuro que voy a crear con esta clase
    self.max = max


  # Método para tener elementos o atributos que voy a necesitar para que el iterador funcione
  def __iter__(self):
    self.num = 0 #Primer número par
    #* Convertir un iterable en un iterador
    return self

  # Método para tener la función "next" de Python
  def __next__(self):
    if not self.max or self.num <= self.max:
      result = self.num
      self.num += 2
      return result
    else:
      raise StopIteration
```

Ventajas de usar iteradores:

1. Nos ahorra recursos.
2. Ocupan poca memoria.

> Iterables -> Todos los objetos que podemos recorrer en un ciclo, ejem: una lista, un string
> Iteradores -> Ahorra recursos, puedo almacenar secuencias y progreciones matematicas, ocupa poca memoria.

## La sucesión de Fibonacci

#### La sucesión de Fibonacci

```python
import time

class FiboIter():
   
   def __init__(selft, max = None):
      if max == None:
         selft.max = None
      else:
         selft.max = max
   
   def __iter__(selft):
      selft.n1 = 0
      selft.n2 = 1
      selft.counter = 0
      return selft
   
   def __next__(selft):
      def fibo(): #funcion fibonacci
         selft.aux = selft.n1 + selft.n2
         #selft.n1 = selft.n2
         #selft.n2 = selft.aux
         selft.n1, selft.n2 = selft.n2, selft.aux
         selft.counter += 1
         return selft.aux
      
      if selft.counter == 0:
         selft.counter += 1
         return selft.n1
      elif selft.counter == 1:
         selft.counter += 1
         return selft.n2
      elif selft.max == None:
         return fibo() #ejecucion infinita
      elif selft.counter >= selft.max:
         raise StopIteration
      else:
         return fibo()#ejecucion controlada

if __name__ == "__main__":
   fibonacci = FiboIter()
   i  = 0
   for element in fibonacci:
      print(str(i) + " Vueltas")
      print(element)
      time.sleep(0.05)
      i += 1
```

[![img](https://www.google.com/s2/favicons?domain=https://es.wikipedia.org/wiki/Sucesi%C3%B3n_de_Fibonacci/static/apple-touch/wikipedia.png)Sucesión de Fibonacci - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Sucesión_de_Fibonacci)

## Generadores

Sugar syntax de los iteradores. Los generadores son funciones que guardan un estado. Es un iterador escrito de forma más simple.

```python
def my_gen():

  """un ejemplo de generadores"""

  print('Hello world!')
  n = 0
  yield n # es exactamente lo mismo que return pero detiene la función, cuando se vuelva a llamar a la función, seguirá desde donde se quedó

  print('Hello heaven!')
  n = 1
  yield n

  print('Hello hell!')
  n = 2
  yield n


a = my_gen()
print(next(a)) # Hello world!
print(next(a)) # Hello heaven!
print(next(a)) # Hello hell!
print(next(a)) StopIteration
```

Ahora veremos un **generator expression** (es como list comprehension pero mucho mejor, porque podemos manejar mucha cantidad
de información sin tener problemas de rendimiento):

```python
#Generator expression

my_list = [0,1,4,7,9,10]

my_second_list = [x*2 for x in my_list] #List comprehension
my_second_gen = ()x*2 for x in my_list]) #Generator expression
```

#### Generator Expression

Es lo mismo que un *iterador* pero escrito de forma más elegante.

Un “list comprehension” es una manera sencilla de implementar un ciclo el cual nos permite crear una nueva lista con elementos basados en los valores de una lista existente. Dicha lista creada almacena cada uno de estos nuevos valores.

A diferencia de un L*ist comprehension* que puede ocupar mucha memoria pues cada valor es almacenado en la variable, un *Generator expression* solo los recorre, sin guardarlos en una lista nueva. Permite traer un elemento a la vez cuando se recorra usando un ciclo **for**.

```python
# Generator expression

my_list = [0, 1, 4, 7, 9, 10]

my_second_list = [x*2 for x in my_list] # List comprehension
my_second_gen = (x*2 for x in my_list)  # Generator expression
```

Como se puede ver en el ejemplo, la diferencia en la sintaxis únicamente es en el uso de paréntesis en lugar del uso de corchetes.

Ventajas de los Generadores:

- Es mas fácil de escribir que un iterador
- Ahorra Tiempo y Memoria
- Permite guardar secuencias infinitas

**solución:**

```python
import time 

def fibo_gen(stop: int):
    n1 = 0
    n2 = 1
    counter = 0

    while True:

        if counter == 0:
            counter += 1
            yield n1

        elif counter == 1:
            counter += 1
            yield n2

        else:
            aux = n1 + n2

            if not stop or aux <= stop:
                n1, n2 = n2, aux
                counter += 1
                yield aux
            else:
                break


if __name__ == '__main__':
    fibonacci = fibo_gen(5)

    for element in fibonacci:
        print(element)
        time.sleep(1)
```

## Sets

Los sets son una estructura de datos muy similares a las listas en cuanto a su forma, pero presentan ciertas características particulares:

- Los sets son **inmutables**
- Cada elemento del set es único, esto es que no se admiten duplicados, aun si durante la definición del set se agregan elementos repetidos pyhton solo guarda un elemento
- los sets guardan los elementos en desorden

Para declararlos se utilizan los {} parecido a los diccionarios solo que carece de la composición de conjunto {a:b, c:d}

```python
# set de enteros
my_set = {1, 3, 5}
print(my_set)

# set de diferentes tipos de datos
my_set = {1.0, "Hi", (1, 4, 7)}
print(my_set)
```

Los sets no pueden ser leídos como las listas o recorridos a través de slices, esto debido a que no tienen un criterio de orden. Sin embargo si podemos agregar o eliminar items de los sets utilizando métodos:

- **add():** nos permite agregar elementos al set, si se intenta agregar un elemento existente simplemente python los ignorara
- **update():** nos permite agregar múltiples elementos al set
- **remove():** permite eliminar un elemento del set, en el caso en que no se encuentre presente dicho elemento, Python elevará un error
- **discard():** permite eliminar un elemento del set, en el caso en que no se encuentre presente dicho elemento, Python dejará el set intacto, sin elevar ningún error.
- **pop():** permite eliminar un elemento del set, pero lo hará de forma aleatoria.
- **clear():** Limpia completamente el set, dejándolo vació.

```python
#ejemplo de operaciones sobre sets 
my_set = {1, 2, 3} 
print(my_set) #Output {1, 2, 3} 

#añadiendo un elemento al set 
my_set.add(4) 
print(my_set) #Output {1, 2, 3, 4}

#añadiendo varios elementos al set, python ignorará elementos repetidos 
my_set.update([1, 5, 6]) 
print(my_set) #Output {1, 2, 3, 4, 5, 6}

# eliminado elementos del set 
my_set.discard(1) 
print(my_set) #Output {2, 3, 4, 5, 6}

# borrando un elemento aleatorio 
my_set.pop()
print(my_set) #Output el set menos un elemento aleatorio 

#limpiar el set 
my_set.clear()
print(my_set) # Output set() 
```

Podemos utilizar estructuras de datos existentes para transformarlas a sets utilizando el método **set**:

```python
#usando listas para crear sets
my_list = [1, 2, 3, 3, 4, 5]
my_set = set(my_list)
print(my_set)  #output {1, 2, 3, 4, 5}

#usando tuplas para crear sets 
my_tuple: ('hola', 'hola', 1, 2)
my_set2: set(my_tuple)
print(my_set2) #Output {'hola', 1
```

## Operaciones con sets

En caso de que quieran hacer operaciones con sets y hacerlo de forma más explícita pueden usar los métodos:

```python
set1.union(set2)
set1.symmetric_difference(set2)
set1.difference(set2)
set1.intersection(set2)
```

Y pueden encontrar más [métodos que pueden serles útiles](https://python-reference.readthedocs.io/en/latest/docs/sets/) 😄

#### Unión

Resultado de juntar todos los elementos que tienen ambos, combinar ambos sets

```python
set_1 = {1, 2, 3}
set_2 = {3, 4, 5}
set_3 = set_1 | set_2 #  -> {1, 2, 3, 4, 5}

# O de forma mas explicita
set_1.union(set_2) #  -> {1, 2, 3, 4, 5}
```

#### Intersección

Los que se repiten en ambos sets

```python
set_1 = {1, 2, 3}
set_2 = {3, 4, 5}
set_3 = set_1 & set_2 #  -> {3}

# O de forma mas explicita
set_1.intersection(set_2) #  -> {3}
```

#### Diferencia

Tomar dos sets y de uno quitarle todos los elementos que tiene el otro

```python
set_1 = {1, 2, 3}
set_2 = {3, 4, 5}
set_3 = set_1 - set_2 #  -> {1, 2}

# O de forma mas explicita
set_1.difference(set_2) #  -> {1, 2}
```

#### Diferencia Simétrica

Tomar todos los elementos exceptos los que se repiten

```python
set_1 = {1, 2, 3}
set_2 = {3, 4, 5}
set_3 = set_1 ^ set_2 #  -> {1, 2, 4, 5}

# O de forma mas explicita
set_1.symmetric_difference(set_2) #  -> {1, 2, 4, 5}
```

### Métodos Set

Una tabla que puede ser útil:

![Selection_839.png](https://static.platzi.com/media/user_upload/Selection_839-5b0a62f8-f2c1-4dc8-a82f-80bc3a6a3779.jpg)

## Eliminando los repetidos de una lista

```python
def remove_duplicates(some_list):
    without_duplicates = []
    for element in some_list:
        if element not in without_duplicates:
            without_duplicates.append(element)
    return without_duplicates

def remove_duplicates_with_sets(some_list):
    return list(set(some_list))

def run():
    random_list = [1, 2, 2, 2, 3, "Platzi", "Platzi", True, 4.6, False]
    print(remove_duplicates(random_list))


if __name__ == '__main__':
    run()
```

**Reto**

```python
import random
import os

def creation_list(min, max, amount):
    my_list = []
    counter = 0
    while counter < amount:
        my_list.append(random.randint(1, 6))
        counter += 1
    return my_list


def remove_duplicate(some_list):
    return list(set(some_list))

def run():
    {os.system("clear")}
    try:
        a = int(input("\tType the minimum of the range: "))
        b = int(input("\tType the maximum of the range: "))
        c = int(input("Type the number of objects in the set: "))
    except TypeError:
        print("You can only use numbers")

    my_list = creation_list(a, b, c)
    print(f"This is your list: {my_list}")
    print(f"This is your list whitout duplicates: {remove_duplicate(my_list)}")


if __name__ == '__main__':
    run()
```

# 5. Bonus

## Manejo de fechas

>  usar `datetime.utcnow()` para usar la hora universal. Nosotros trabajamos con equipos de todo el mundo, no podemos usar hora local.

`datetime` es un módulo de manejo de fechas.

```python
import datetime

my_time = datetime.datetime.now() # hora local de mi PC u hora universal
my_date = datetime.date.today() # fecha actual

my_day = datetime.date.today()

print(my_time)
print(my_date)

print(f'Year: {my_day.year}')
print(f'Month: {my_day.month}')
print(f'Day: {my_day.day}')
```

Tabla de códigos de formato para fechas y horas(los más importantes):

| Formato  | Código |
| -------- | ------ |
| Año      | %Y     |
| Mes      | %m     |
| Día      | %d     |
| Hora     | %H     |
| Minutos  | %M     |
| Segundos | %S     |

```python
from datetime import datetime

my_datetime = datetime.now()
print(my_datetime)

latam = my_datetime.strftime('%d/%m/%Y')
print(f'Formato LATAM: {latam}')

usa = my_datetime.strftime('%m/%d/%Y')
print(f'Formato USA: {usa}')

random_format = my_datetime.strftime('año %Y mes %m día %d')
print(f'Formato random: {random_format}')

formato_utc = datetime.utcnow()
print(f'Formato UTC: {formato_utc}')
```

## Time zones


```py
from datetime import datetime
import pytz

def get_date_time_from_timezone(city_name:str, timezone:str)-> str:
  city_timezone = pytz.timezone(timezone)
  city_time = datetime.now(city_timezone)
  print(f'{city_name}: {city_time.strftime("%d/%m/%Y, %H:%M:%S")}')


get_date_time_from_timezone("London", "Europe/London")
get_date_time_from_timezone("LA", "America/Los_Angeles")
get_date_time_from_timezone("CDMX", "America/Mexico_City")
get_date_time_from_timezone("Tokyo", "Asia/Tokyo")
get_date_time_from_timezone("Sydney", "Australia/Sydney")
```

[List of tz database time zones - Wikipedia](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)

# 6. Conclusión

## Completaste la trilogía. ¿Cómo seguir?

Never stop Learnig!